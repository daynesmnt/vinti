import {useState,useEffect} from 'react';
import { Pie } from 'react-chartjs-2';
import moment from 'moment';

import Alert from 'react-bootstrap/Alert';

export default function PieChart({dataIncome,dataExpense,label}){
console.log(dataExpense)
console.log(dataIncome)
const types = ["Income","Expenses"]

const data = {
		labels:types, //this will be the labels of each part of the pie chart
		datasets: [
			{
				data:[dataIncome,dataExpense], // this will be an array of numbers to be represented in our pic chart 
				backgroundColor:["#8B4513","indianred"],
				hoverBackgroundColor:["pink","pink"], // this will be like our background color, will need an array of colors to show when we are hovering a part of the pie .
			}
		]
	}
	return(
		<>
		<h1>Income Vs Expense</h1>
		<Pie data={data}></Pie>

		</>
		
		)
}