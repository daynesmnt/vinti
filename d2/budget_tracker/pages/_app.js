import {useState,useEffect,Fragment} from 'react';
// css bs
import 'bootstrap/dist/css/bootstrap.min.css';

import '../styles/globals.css'

import {Container} from 'react-bootstrap';
import NavBar from '../components/NavBar';
import { UserProvider } from '../UserContext';

export default function MyApp({ Component, pageProps }) {
	const [user,setUser]=useState({
		//initial value for user is null
		id:null
	})
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{

				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data => {
			//console.log(data)

			if(data._id){

				setUser({
					id:data._id
				})
			}else{
				setUser({
					id:null
				})
			}
		})
	},[user.id])
	const unsetUser = () => {
		localStorage.clear()
		setUser({
			id:null
		})
	}
	//console.log(user.id)

  return(
  	<Fragment>
	  	<UserProvider value={{user,setUser,unsetUser}}>
		  	<NavBar/>
		  	<Container>
		  		
		  		<Component {...pageProps} />
		  	</Container>
	  	</UserProvider>
  	</Fragment>

  	)
}


