import {useEffect,useState,useContext} from 'react';
import moment from 'moment';
import {Tabs, Tab} from 'react-bootstrap';
import MonthlyIncome from '../components/MonthlyIncome'
import MonthlyExpense from '../components/MonthlyExpense'
import PieChart from '../components/PieChart'
import LineChart from '../components/LineChart'
import UserContext from '../UserContext' 
import Head from 'next/head';
export default function insights(){

	const [allWallet,setAllWallet] = useState([])
	const { user } = useContext(UserContext)

	const [incomeSum,setIncomeSum]= useState(0)
	const [expenseSum,setExpenseSum]= useState(0)
	

	const [monthlyIncome,setMonthlyIncome] =useState([])
	const [monthlyExpense,setMonthlyExpense] =useState([])
	//console.log(user)
	// array of all inocme, expenses record
	const [incomeTotal,setIncomeTotal] =useState([])
	const [expenseTotal,setExpenseTotal]= useState([])

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{

				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data => {
		//	console.log(data)
			setAllWallet(data.wallet)
		})
	},[])
		


		useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{

				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data => {
			//console.log(data)

			const allWallet =data.wallet
			//console.log(allWallet)
			const allCategories = data.categories
			//console.log(allCategories)
			//labels for category pie
			const allCategoryNames = allCategories.map(element=>{
				return element.name
			})
			//returns all category names for muser aaray not wallet array 
			//console.log(allCategoryNames)
		/*	
			allCategoryNames.map(name=>{
				
				console.log(name)
				const dataPie = allWallet.filter(function(element){
					return element.categoryName	== name	
				})

				setDataPie(dataPie)
			})
			//console.log(name)
			const walletFilteredCategories = allWallet.filter(function(element){
				return element.categoryName	== name[0]	
			})
			

*/
			
		//console.log(name)

			//shall retrun all records containing same categoryNames
			//console.log(walletFilteredCategories)
		
			const allInomeRecords = allWallet.filter(function(element){
			return element.type == "Income";
			})
			const allExpenseRecords = allWallet.filter(function(element){
			return element.type == "Expense";
			})

			const incomeTotal = allInomeRecords.map(element =>{
			return element.amount
			})
			setIncomeTotal(incomeTotal)
			const expenseTotal = allExpenseRecords.map(element =>{
			return element.amount
			})
			setExpenseTotal(expenseTotal)
			const expenseSum =expenseTotal.reduce(function(a,b){
			return a+b;
			},0);
			const incomeSum =incomeTotal.reduce(function(a,b){
			return a+b;
			},0);


			setIncomeSum(incomeSum)
			setExpenseSum(expenseSum)

			if(data.wallet.length>0){
				let monthlyIncome = [0,0,0,0,0,0,0,0,0,0,0,0]
				let monthlyExpense = [0,0,0,0,0,0,0,0,0,0,0,0]
				data.wallet.forEach(wallet =>{
					const index = moment(wallet.recordedOn).month()

					//console.log(index)
					// wallet.amount should be an array of all income only
					monthlyIncome[index] += (parseInt(incomeTotal))
					monthlyExpense[index] += (parseInt(expenseTotal))

					//console.log(monthlyIncome)
					//console.log(monthlyExpense)
					setMonthlyIncome(monthlyIncome)
					setMonthlyExpense(monthlyExpense)
				})
			}




		})
	},[])


		/*

			if(data.wallet.length>0){
				let monthlyIncome = [0,0,0,0,0,0,0,0,0,0,0,0]
				let monthlyExpense = [0,0,0,0,0,0,0,0,0,0,0,0]
				data.wallet.forEach(wallet =>{
					const index = moment(wallet.recordedOn).month()

					console.log(index)
					// wallet.amount should be an array of all income only
					monthlyIncome[index] +=(incomeTotal)
					monthlyExpense[index] +=(expenseTotal)

					console.log(monthlyIncome)
				})
			}



		*/


/*	console.log(allWallet)
	const allInomeRecords = allWallet.filter(function(element){
		return element.type == "income";
	})
	const allExpenseRecords = allWallet.filter(function(element){
		return element.type == "expense";
	})

	console.log(allExpenseRecords)
	console.log(allInomeRecords)

	const incomeTotal = allInomeRecords.map(element =>{
		return element.amount
	})
	console.log(incomeTotal)
	const expenseTotal = allExpenseRecords.map(element =>{
		return element.amount
	})
	console.log(expenseTotal)

	data={incomeTotal}

*/

	return(

		<>
		<Head>
		<title>Insights || Vinti</title>
		</Head>

		<Tabs>
			<Tab eventKey='income' title='MonthlyIncome'>
				<MonthlyIncome label="Monthly Income by months" dataIncome={monthlyIncome} />
			</Tab>
			<Tab eventKey='expense' title='Monthly Expense'>
				<MonthlyExpense label="Monthly Expense by months" dataExpense={monthlyExpense}/>
			</Tab>
			<Tab eventKey='pieChart' title='Income Vs Expense Analysis'>
				<PieChart label="Income Vs Expense" dataIncome={incomeSum} dataExpense={expenseSum} />
			</Tab>
			<Tab eventKey='lineChart' title='BalanceTrend'>
				<LineChart label="Income" dataIncome={monthlyIncome} dataExpense={monthlyExpense} incomeArray={incomeTotal} expenseArray={expenseTotal} />
			</Tab>

		</Tabs>
		</>
		)

}
