import Head from 'next/head';
import {Container} from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';
import Router from 'next/Router';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
export default function Home(){


  return(

	 <>

	   <Head><title>Home || Vinti</title></Head>

        <Container className="landingPage">
          <Row>
            <Col>
                 <h1 className="landingTitle">Vinti</h1>
                 <p className="landingText">Developed by: <a href="https://daynesmnt.gitlab.io/samonte-dayne-portfolio/">Dayne Samonte</a></p>
                  <p className="landingText"> A Vintage Budget Tracker to help organize and visualize your money</p>
            </Col>
          </Row>

        </Container>
        <Container className="infoBar">
          <Row>
            <Col>
                 <h3 className="infoTitle">What is Vinti?</h3>
                  <p className="infoText"> Vinti is a vintage budget tracker wherein you have your own wallet that can organize your income and expenses. You can set your own categories and add records according to your preferred category. On the Insights page, you can track the monthly balance of your wallet.  </p>
                  <h3 className="infoTitle">How to use?</h3>
                  <p className="infoText">It's very simple, just add categories on the Categories page, you can add as many as you want. Then, go to Records and select your Category name and if it is an Income or Expense Record. Then just add an amount and a Descriptiond to destinguish your records. Afterwards, you can search and filter your records based on a type, or on a category. </p>
                    <h3 className="infoTitle">What is the Insights page?</h3>
                  <p className="infoText">Insights is a collection of data analysis based on the records you have added. In each tab inside insights will be an interpretation of your records. </p>

            </Col>
          </Row>

        </Container>

        <Navbar fixed="bottom" className="infoTitle"> Dayne Samonte &copy; October 2020 </Navbar>






    
  </>
    )
}
