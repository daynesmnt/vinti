const mongoose = require("mongoose")
const userSchema = new mongoose.Schema({

	email: {
		type: String,
	},

	password:{
		type:String,
	}, 
	loginType:{
	 	type:String,
	},
	categories:[
		{
			name:{
				type:String,
			},
				
			type:{
				type: String,	
			}


		}
	],
	wallet:[
		{
			
			recordedOn:{
				type: Date,
				default: new Date()
			},
			categoryName:{
				type:String
			},
			type:{
				type:String

			},
			description:{
				type:String
			},
			amount:{
				type:Number
			}

		}
	]

})
module.exports = mongoose.model("User", userSchema)