const jwt = require("jsonwebtoken")
const secret = "MySecret" // secret can be any phrase

module.exports.createAccessToken = (user) => 
// the user here comes from  the log in .. contains all the info about the user 
{
	const data = {
		id : user._id,
		email : user.email,
	}

	return jwt.sign(data,secret, {});
}

// recieving the gif | check if reciever is legit

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization // we put the JWT in the headers request.. in postman this is in authorization -> select "Bearer token" -> Token
	if(typeof token !== "undefined"){
		// the token exists
		token = token.slice(7, token.length)
		// string .slice - cuts the string starting from the vaue up to the specified value/ starts at 7 and ends at the last character
		// strings are arrays of characters
		// the first 7 characters are not relevant / related to the actual data we need 
		return jwt.verify(token,secret,(err,data) => {
			// jwt.verify verifies the token with the secret and fails if the token's secret doesnt match with the secret phrase(ie the token has been tampered with)
				return (err) ? res.send ({auth : "failed"}) : next()
				//next() is a function that allows us to proceed to the next request
		})
	} else {
		return res.send({auth : "failed"})

		}
	}

	// decode token || opening the gift 
	module.exports.decode = (token) => {
		if(typeof token !== "undefined"){
			token = token.slice(7, token.length)
			return jwt.verify(token, secret, (err, data) => {
				return (err) ? null : jwt.decode(token, {complete:true}).payload
				//jwt.decode decodes the token and gets the payload . payload in this case contains the data from the createAccessToken 
			})
		} else {
			return null
		}
	}