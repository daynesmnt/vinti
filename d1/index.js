/*dependencies set up*/
const express = require("express")
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require("./routes/user")
const categoryRoutes = require("./routes/category")
const recordRoutes = require("./routes/record")

/*mongoose connection*/
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://database_admin:1amadmin@cluster0.gt5to.mongodb.net/Budget_Tracker?retryWrites=true&w=majority', { 
	useNewUrlParser: true, 
	useUnifiedTopology: true 
})
/*server set up*/
const app = express()
const port = 4000
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors()) /*<-- this will be used if other sites would use this API*/


/*All routes*/

app.use("/api/users",userRoutes)

/*app.use("/api/categories",categoryRoutes)

app.use("/api/records",recordRoutes)
*/
/*server listening*/
app.listen(port,() => console.log(`Listening to port ${port}`))


