const User = require("../models/user")


const bcrypt = require("bcrypt")

const auth = require("../auth")// is used for authorization of requests
const { OAuth2Client } = require('google-auth-library')
const clientId = '928010259562-r5tgl0jlaf9inqcka0lrjvso76bvt3m8.apps.googleusercontent.com'

const nodemailer = require('nodemailer'); // this is node js package which can be used to sen mails
const { google } = require("googleapis");
const OAuth2 = google.auth.Oauth2;// create a new oath2 from google to give authorization to send emails to our google user

require('dotenv').config()// to manage our environmentvariables
module.exports.emailExists = (parameterFromRoute) => {
	// step 1 : Use mongoose's find function using email
	// 2. the then() expects a result or response from the find()
	// 3. use the result from find and check if the length is 0 or not
	// 4. the ternary operator returns either true or false
	return User.find({email : parameterFromRoute.email }).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false
	})
		// params.email would come from the request when we use the controller n the routes
}


module.exports.register = (params) => {
	let newUser = new User({
		email : params.email,
		password : bcrypt.hashSync(params.password,10)
		// hashSync() hashes/encrypt and 20 is salt, it hashes 10 times
		})
	return newUser.save().then((user, err) => {
		return(err) ? false : true
	})
}

//category part

module.exports.addCategory = (params) => {
	return User.findById(params.userId).then(resultFromFindByIdUser =>{
		resultFromFindByIdUser.categories.push({name: params.name,
		type: params.type})
		return resultFromFindByIdUser.save().then((resultFromSaveUser,err)=>{
			return(err)? false:true
		})
	})
}


module.exports.addWallet = (params) => {
	return User.findById(params.userId).then(resultFromFindByIdUser =>{
		resultFromFindByIdUser.wallet.push({
			walletId:params.walletId,
			categoryName: params.categoryName,
			type: params.type,
			description: params.description,
			amount: params.amount
		})
		//console.log(params)
		return resultFromFindByIdUser.save().then((resultFromSaveUser,err)=>{
			return(err)? false:true
		})
	})
}


/*
module.exports.updateWallet = (params) => {
	let updatedWallet = {
		categoryName:params.categoryName,
		description:params.description,
		type:params.type,
		amount:params.amount
	}
	return User.findByIdAndUpdate({_id:params.userId,_id:params.walletId},{updatedWallet}).then((wallet,err)=>{
		return(err) ? false : console.log(wallet)
	})
	
	
}
	*/
	

/*module.exports.delete = (requested) => {
	let params = JSON.stringify(requested)
	let updatedWallet = {
		categoryName:params.categoryName,
		description:params.description,
		type:params.type,
		amount:params.amount
	}
	console.log("*** updateParams : "+ params)

	User.findOneAndDelete(params.walletId,updatedWallet).then((wallet,err)=>{
		return(err) ? false : true
	})

	
}*/



/*	let updatedWallet={
		categoryName:params.categoryName,
		description:params.description,
		type:params.type,
		amount:params.amount

	}*/










module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(resultFromFindOne => {
		if (resultFromFindOne == null){ // user doesnot exist
				return false

		}
		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)
		//compareSync() is used compare a nonhashed password to a hashed password, this returns a boolean (true or false)
		// a good coding practice in naming a boolean variables is in form of is<Noun> ex: isLegal , isPasswordMatched, isDone
		// at this point, there is a user and the passwords are compared  
		if(isPasswordMatched){
			return{ access : auth.createAccessToken(resultFromFindOne.toObject())}
			// auth is custom module that uses JWT to pass data around
			// createAccessToken is a method in auth that create a JWT with the user as part of the token 
		} else {
			return false
		}
	})
}
module.exports.get = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.password = undefined
			// why is it undefined? to hide the password
			//console.log(params)
		return resultFromFindById
	})
}

module.exports.getAll = () => {
	return User.find().then(resultFromFind => resultFromFind)
	// an empty find will return all the categorys 
}

//from our client we are going to send our google user's token id andAccessTken
// token id to verify our User 
// access token so we can be authorized to send email to our user
module.exports.verifyGoogleTokenId = async (body) => {

	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({idToken:body.tokenId, audience:clientId})

	if(data.payload.email_verified === true){
		const user = await User.findOne({email: data.payload.email}).exec()

		if(user !== null){
			console.log('A user has been registered')
			if (user.loginType === 'google'){
				// if else statement to check if the use loging in, used a google log in to register for the first time. 
			return {access:auth.createAccessToken(user.toObject())}
			}else{
				return { error: 'login-type-error'}

			}
		}else {
			//check if data.payload is coming in
			console.log(data.payload)
			//register our new user into db with details responded by google
			const newUser = new User({
				firstname: data.payload.given_name,
				lastname: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			})
			
			return newUser.save().then((user,err)=>{
				console.log(user)
				// send an email to the user thanking them for registering into our app
				// mailOptions : the content of the email we're going to send our user. This will include the message , the email address of the sender and the email address of the recipient. 
				const mailOptions = {
					from: 'daynesmnt@gmail.com', // the email you are going to send from
					to: user.email,// the email of the user
					// we got this from our user object returned as a response after we saved our new user. 
					subject: "Thank you for registering to Vinti",
					// the subject of your email
					text:`You have registered to Vinti, a budget tracking site on ${new Date().toLocaleString()}`,
					html:`You have registered to Vinti on ${new Date().toLocaleString()}`


				}
				//to pass details to our nodemailer, we have to trasport the needed auithorization to send an email to our user. 
				const transporter = nodemailer.createTransport({
					host:'smtp.gmail.com',
					port:465,
					secure:true,
					auth:{
						type:'OAuth2',
						user: process.env.MAILING_SERVICE_EMAIL,
						clientId: process.env.MAILING_SERVICE_CLIENT_ID,
						clientSecret:process.env.MAILING_SERVICE_CLIENT_SECRET,
						refreshToken:process.env.MAILING_SERVICE_REFRESH_TOKEN,
						accessToken: body.googleToken
					}

				})

				//create a function to send our email
				//uses the transporter as a params to send our email with the proper authorizations
				// 
				function sendMail(transporter){
					transporter.sendMail( mailOptions,function(err,result){
						if(err){
							console.log(err)
						} else if(result){
							console.log(result)
							transport.close()
						}
					} )
				}
				sendMail(transporter)
				return {access:auth.createAccessToken(user.toObject())}
				// if a new google user logins for the firsttime, he is registered to our database and once he/she has been registered, well be returning a token
			})
		}

		// 
	}else{
		//an eeror in checking token
		return { error:'google-auth-error' }

	}
}
