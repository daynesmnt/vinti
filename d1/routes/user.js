const express = require("express")
const router = express.Router()
const auth = require("../auth")


const UserController = require("../controllers/user")

router.get("/", (req,res)=>{
	UserController.getAll().then(result => res.send(result))
})

router.post("/email-exists",(req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExist => res.send(
		resultFromEmailExist))
})
/*router.put("/categories", (req,res) =>{
	 UserController.addCategory(req.body).then(resultFromAdd => res.send(resultFromAdd))
	 console.log(req)
})

router.post("/records", (req,res) =>{
	 UserController.addRecord(req.body).then(resultFromAdd => res.send(resultFromAdd))
	 console.log(req)
})
*/
//newcategry
router.post("/newCategory",auth.verify,(req,res)=>{
	let params ={
		userId:auth.decode(req.headers.authorization).id,
		name:req.body.name,
		type:req.body.type
	}
	UserController.addCategory(params).then(resultFromNewCat => res.send(resultFromNewCat))
})
//new wallet
router.post("/newWallet",auth.verify,(req,res)=>{
	let params ={
		userId:auth.decode(req.headers.authorization).id,
		
		categoryName:req.body.categoryName,

		type:req.body.type,

		description:req.body.description,

		amount:req.body.amount
	}
	UserController.addWallet(params).then(resultFromNewWallet => res.send(resultFromNewWallet))
})

/*
router.post("/:walletId",(req,res)=>{
	let walletId = req.params.walletId
	//console.log(req.body)
	//console.log("REQUESTBODYY@@@@@@2" + JSON.stringify(req.body))
	UserController.updateWallet(req.body).then(result => res.send(result))
})

*/



router.post("/",(req,res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

router.post("/login",(req,res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})
router.get("/details",auth.verify,(req,res)=>{
	//get the decoded token, the decoded token already has some details from the user
	const user = auth.decode(req.headers.authorization);
	// We use id of the user from the token to search for additional information of the user 
	UserController.get({userId : user.id}).then(user => res.send(user));
})
router.post('/verify-google-id-token', async (req,res) =>{
	res.send(await UserController.verifyGoogleTokenId(req.body))
})

module.exports = router